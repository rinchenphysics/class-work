let form = document.querySelector("#taskform");
let input = document.querySelector("#task-input");
let tasks = document.querySelector("#tasks");

form.addEventListener("submit", (event) => {
    
    event.preventDefault();
    
    let x = input.value;
    
    if (!x) {
        alert("You can not leave it blank!")        
    } else {
        let y = document.createElement("div");
        let z = document.createElement("input");
        z.type = "text";
        z.value = x;
        z.readOnly = true;
        let f = document.createElement("div");

        let ediB = document.createElement("Button");
        let edit = document.createTextNode("Edit");
        ediB.appendChild(edit);

        let delB = document.createElement("Button");
        let del = document.createTextNode("Delete");
        delB.appendChild(del);
        
        f.appendChild(z);
        f.appendChild(delB);
        f.appendChild(ediB);
        y.appendChild(f);
        tasks.appendChild(y);
        
        delB.addEventListener("click", ()=>{
          
            tasks.removeChild(y);

        })
        ediB.addEventListener("click", ()=>{
          
            z.readOnly = false; 

        })

       
    }
});